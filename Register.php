<!DOCTYPE html>
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Register</title>
<body>

<form action='' method="post">
        <div style='padding-top:30px'>
            <span style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Họ và tên</span>
            <input style='border:2px solid #FEAFA2;
                            padding:5px 0px'>
        </div>
        
        <div style='padding-top:30px'>
            <span style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Giới tính</span>
                    <?php
                    $gender = array(0 => "Nam", 1 => "Nữ"); 
                    for ($i = 0; $i < count($gender); $i++){ ?> 
                        <input type="radio" name="gender" id= <?= $i ?>/>
                        <label for="<?= $i ?>"> <?= $gender[$i] ?></label>
                    <?php }
                    ?>
        </div>
        
        <div style='padding-top:30px'>
            <span style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Phân khoa</span>
            <select name="depart" id="depart" style='border:2px solid #FEAFA2; padding:5px 0px'>
            	<?php
                $depart = array("null" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                foreach ($depart as $key => $value) { ?>
                	<option value="<?=$key?>"><?=$value ?></option>
                <?php }
                ?>
            </select>
        </div>
        
        <div style='margin-top:20px;margin-left:100px'>
            <button style='background-color:#FEA6B6;
                        padding:10px 20px;
                        border-radius:8px;
                        border-color:#FEA6B6'>Đăng ký</button>
        </div>
        
</form>
</body>
</html>
